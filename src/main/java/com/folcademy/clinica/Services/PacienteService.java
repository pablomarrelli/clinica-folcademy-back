package com.folcademy.clinica.Services;

import com.folcademy.clinica.Model.Dtos.MedicoDto;
import com.folcademy.clinica.Model.Dtos.PacienteDto;
import com.folcademy.clinica.Model.Entities.Medico;
import com.folcademy.clinica.Model.Entities.Paciente;
import com.folcademy.clinica.Model.Mappers.PacienteMapper;
import com.folcademy.clinica.Model.Repositories.PacienteRepository;
import com.folcademy.clinica.Exception.NotFoundException;
import com.folcademy.clinica.Exception.ValidationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class PacienteService {
    private final PacienteRepository pacienteRepository;
    private final PacienteMapper pacienteMapper;

    public PacienteService(PacienteRepository pacienteRepository, PacienteMapper pacienteMapper) {
        this.pacienteRepository = pacienteRepository;
        this.pacienteMapper = pacienteMapper;
    }


    public Page<PacienteDto> ListaPacientes(Integer pageNumber, Integer pageSize, String orderField){
        // Page<Medico> medicos = (Page<Medico>) medicoRepository.findAll();
        //return medicos.stream().map(medicoMapper::entityToDto).collect(Collectors.toList());
        Pageable pageable= PageRequest.of(pageNumber, pageSize, Sort.by(orderField));
        return pacienteRepository.findAll(pageable).map(pacienteMapper::entityToDto);
        // return medicoRepository.findAll().stream().map(medicoMapper::entityToDto).collect(Collectors.toList());
    }
    public Page<PacienteDto>findById(Integer id, Integer pageNumber, Integer pageSize, String orderField)
    {
        //Optional<Paciente> busca = pacienteRepository.findById(id);
        //if (busca.isPresent())
        if (!pacienteRepository.existsById(id))
            throw new NotFoundException("No existe el Paciente con id " + id);
        else
        {
            PacienteDto dto = pacienteMapper.entityToDto(pacienteRepository.findById(id).get());
            Pageable pageable= PageRequest.of(0,1);
            return pacienteRepository.findById(dto.getId(),pageable).map(pacienteMapper::entityToDto);}
    }


/*    public List<Paciente> findPacienteById(Integer id) {
        List<Paciente> lista = new ArrayList<>();
        Paciente paciente = pacienteRepository.findById(id).get();
        lista.add(paciente);
        return lista;
    }*/


    public PacienteDto agregar(PacienteDto paciente){
        paciente.setId(null);
        if(paciente.getPersona().getDni().isEmpty()|| paciente.getPersona().getNombre().isEmpty()|| paciente.getPersona().getApellido().isEmpty())
            throw new ValidationException("Debe ingresar todos los datos del paciente");
        return  pacienteMapper.entityToDto(pacienteRepository.save(pacienteMapper.dtoToEntity(paciente)));
    }

    public ResponseEntity<String> borrar(Integer id)
    {
        Optional<Paciente> valida = pacienteRepository.findById(id);
        if(valida.isPresent()){
        pacienteRepository.deleteById(id);
        return new ResponseEntity<>("Paciente eliminado", HttpStatus.OK);}
        else
            throw new NotFoundException("Paciente no encontrado");
    }

    public ResponseEntity<String> update(Integer id, PacienteDto modifica)
    {
        Optional<Paciente> updateOptional = pacienteRepository.findById(id);

        if(updateOptional.isPresent()){
            Paciente update = updateOptional.get();
            if (modifica.getPersona().getDni().isEmpty())
                throw new ValidationException("Debe ingresar el dni");
             else update.setDni(modifica.getPersona().getDni());
             if (modifica.getPersona().getNombre().isEmpty())
                 throw new ValidationException("Debe ingresar el nombre");
            else update.setNombre(modifica.getPersona().getNombre());
            if (modifica.getPersona().getApellido().isEmpty())
                throw new ValidationException("Debe ingresar apellido");
            else update.setApellido(modifica.getPersona().getApellido());
            if (modifica.getPersona().getTelefono().isEmpty())
                throw new ValidationException("Debe ingresar telefono");
            else
            update.setTelefono(modifica.getPersona().getTelefono());
            if (modifica.getDireccion().isEmpty())
                throw new ValidationException("Debe ingresar direccion");
            else
                update.setDireccion(modifica.getDireccion());

            pacienteRepository.save(update);
            return new ResponseEntity<>("Se actualizo", HttpStatus.OK);
        }else{
            return new ResponseEntity<>("Paciente no existente", HttpStatus.NOT_FOUND);
        }

    }
    public Page<PacienteDto> listarTodosByPage(Integer pageNumber, Integer pageSize, String orderField) {
        Pageable pageable= PageRequest.of(pageNumber, pageSize, Sort.by(orderField));
        return pacienteRepository.findAll(pageable).map(pacienteMapper::entityToDto);
    }
}
