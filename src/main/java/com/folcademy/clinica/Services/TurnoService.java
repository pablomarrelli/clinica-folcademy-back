package com.folcademy.clinica.Services;

import com.folcademy.clinica.Exception.NotFoundException;
import com.folcademy.clinica.Exception.ValidationException;
import com.folcademy.clinica.Model.Dtos.MedicoDto;
import com.folcademy.clinica.Model.Dtos.PacienteDto;
import com.folcademy.clinica.Model.Dtos.TurnoDto;
import com.folcademy.clinica.Model.Entities.Paciente;
import com.folcademy.clinica.Model.Entities.Turno;
import com.folcademy.clinica.Model.Mappers.TurnoMapper;
import com.folcademy.clinica.Model.Repositories.TurnoRepository;

import com.folcademy.clinica.Services.Interfaces.ITurnoService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class TurnoService implements ITurnoService {

    private final TurnoRepository turnoRepository;
    private final TurnoMapper turnoMapper;


    public TurnoService(TurnoRepository turnoRepository, TurnoMapper turnoMapper) {
        this.turnoRepository = turnoRepository;
        this.turnoMapper = turnoMapper;
    }

    public Page<TurnoDto> findAll(Integer pageNumber, Integer pageSize, String orderField){
        // Page<Medico> medicos = (Page<Medico>) medicoRepository.findAll();
        //return medicos.stream().map(medicoMapper::entityToDto).collect(Collectors.toList());
        Pageable pageable= PageRequest.of(pageNumber, pageSize, Sort.by(orderField));
        return turnoRepository.findAll(pageable).map(turnoMapper::entityToDto);
    }
      /*public List<Turno> findTurnoById(Integer id) {
        List<Turno> lista = new ArrayList<>();
        Optional<Turno> turno = turnoRepository.findById(id);
        if (turno.isPresent())
        {

        }else
        lista.add(turno);
        return lista;
    }
*/
      public Page<TurnoDto> findById(Integer id,Integer pageNumber, Integer pageSize, String orderField)
      {
          //Optional<Turno> busca = turnoRepository.findById(Integer id, Integer pageNumber, Integer pageSize, String orderField);
          if (!turnoRepository.existsById(id)) {
              throw new NotFoundException("Paciente no encontrado");
          } else {
              TurnoDto dto = turnoMapper.entityToDto(turnoRepository.findById(id).get());
              Pageable pageable= PageRequest.of(0,1);
              return turnoRepository.findById(dto.getId(),pageable).map(turnoMapper::entityToDto);
          }

          /*if (busca.isPresent())
              return turnoRepository.findById(id).map(turnoMapper::entityToDto).orElse(null);
          else
              throw new NotFoundException("No existe el turno con id " + id);*/
      }
    public TurnoDto insertar(TurnoDto turno){
        turno.setId(null);
        if(turno.getIdmedico() == null || turno.getIdpaciente() == null)
            throw new ValidationException("Debe ingresar todos los datos del paciente");
        return  turnoMapper.entityToDto(turnoRepository.save(turnoMapper.dtoToEntity(turno)));
    }

    public ResponseEntity<String> borrar(Integer id)
    {
        Optional<Turno> valida = turnoRepository.findById(id);
        if(valida.isPresent()){
            turnoRepository.deleteById(id);
            return new ResponseEntity<>("Turno eliminado", HttpStatus.OK);}
        else
            throw new NotFoundException("Turno no encontrado");
    }

    public ResponseEntity<String> update(Integer id, TurnoDto modifica)
    {
        Optional<Turno> updateOptional = turnoRepository.findById(id);

        if(updateOptional.isPresent()){
            Turno update = updateOptional.get();
            if (modifica.getFecha() == null)
                throw new ValidationException("Debe ingresar la fecha");
            else update.setFecha(modifica.getFecha());
            if (modifica.getHora() == null)
                throw new ValidationException("Debe ingresar la hora");
            else update.setFecha(modifica.getFecha());
           if (!modifica.getAtendido() || modifica.getAtendido())
               update.setAtendido(modifica.getAtendido());
            else throw new ValidationException("Debe ingresar si se atendio");
            /*if (modifica.getIdpaciente() < 0)
                throw new ValidationException("Debe ingresar id paciente");
            else update.setPaciente(modifica.getIdpaciente());
            if (modifica.getIdmedico() < 0)
                throw new ValidationException("Debe ingresar id medico");
            else
                update.setIdmedico(modifica.getIdmedico());*/

            turnoRepository.save(update);
            return new ResponseEntity<>("Se actualizo", HttpStatus.OK);
        }else{
            return new ResponseEntity<>("Turno no existente", HttpStatus.NOT_FOUND);
        }

    }

}
