package com.folcademy.clinica.Services;

import com.folcademy.clinica.Model.Dtos.MedicoDto;
import com.folcademy.clinica.Model.Dtos.PacienteDto;
import com.folcademy.clinica.Model.Entities.Medico;
import com.folcademy.clinica.Model.Entities.Persona;
import com.folcademy.clinica.Model.Mappers.MedicoMapper;
import com.folcademy.clinica.Model.Repositories.MedicoRepository;
import com.folcademy.clinica.Exception.BadRequestException;
import com.folcademy.clinica.Exception.NotFoundException;
import com.folcademy.clinica.Exception.ValidationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service("medicoService")
public class MedicoService {
    private final MedicoRepository medicoRepository;
    private final MedicoMapper medicoMapper;


    public MedicoService(MedicoRepository medicoRepository, MedicoMapper medicoMapper) {
        this.medicoRepository = medicoRepository;
        this.medicoMapper = medicoMapper;
    }

    public Page<MedicoDto> listarTodos(Integer pageNumber, Integer pageSize, String orderField){
       // Page<Medico> medicos = (Page<Medico>) medicoRepository.findAll();
        //return medicos.stream().map(medicoMapper::entityToDto).collect(Collectors.toList());
        Pageable pageable= PageRequest.of(pageNumber, pageSize, Sort.by(orderField));
        return medicoRepository.findAll(pageable).map(medicoMapper::entityToDto);
        // return medicoRepository.findAll().stream().map(medicoMapper::entityToDto).collect(Collectors.toList());
    }


    public Page<MedicoDto> listarUno(Integer id, Integer pageNumber, Integer pageSize , String orderField){

        if (!medicoRepository.existsById(id))
            throw new NotFoundException("No exite el medico con id " + id);
        else{
            MedicoDto dto = medicoMapper.entityToDto(medicoRepository.findById(id).get());
            Pageable pageable =PageRequest.of(0, 1);
           return medicoRepository.findByIdMedico(dto.getIdMedico(), pageable).map(medicoMapper::entityToDto);}

    }

    public MedicoDto agregar(MedicoDto entity){
        entity.setIdMedico(null);
        if (entity.getConsulta() <= 0)
            throw new BadRequestException("La consulta no puede ser menor a 0");
        return medicoMapper.entityToDto(medicoRepository.save(medicoMapper.dtoToEntity(entity)));
    }

    public ResponseEntity<String> borrar(Integer id){

        Optional<Medico> valida = medicoRepository.findById(id);
        if (valida.isPresent()){
            medicoRepository.deleteById(id);
            return new ResponseEntity<>("Medico eliminado", HttpStatus.OK);
        }else
        throw new NotFoundException ("Medico no encontrado");
    }
    /*public ResponseEntity<String> updateProfesion(Integer id, String modifica)
    {
        Optional<Medico> updateOptional = medicoRepository.findById(id);

        if (updateOptional.isPresent()){
            Medico update = updateOptional.get();
            update.setProfesion(modifica);
            medicoRepository.save(update);
            return new ResponseEntity<>("Se actualizo la profesion", HttpStatus.OK);
        }else {
            return new ResponseEntity<>("No se encontro", HttpStatus.NOT_FOUND);
        }
    }
*/
    public ResponseEntity<String> update(Integer id, Medico modifica)
    {
        Optional<Medico> updateOptional = medicoRepository.findById(id);

        if (updateOptional.isPresent()){
            Medico update = updateOptional.get();
            if (modifica.getNombre().isEmpty())
                throw new ValidationException("Debe ingresar el nombre");
            else update.setNombre(modifica.getNombre());
            if(modifica.getApellido().isEmpty())
                throw new ValidationException("Debe ingresar el apellido");
            else update.setApellido(modifica.getApellido());
            if(modifica.getProfesion().isEmpty())
                throw new ValidationException("Debe ingresar la profesion");
            else update.setProfesion(modifica.getProfesion());
            if (modifica.getConsulta() <= 0 )
                throw new ValidationException("La consulta debe ser mayor a 0");
            else update.setConsulta(modifica.getConsulta());
            if (modifica.getTelefono().isEmpty())
                throw new ValidationException("Debe ingresar telefono");
            else
                update.setTelefono(modifica.getTelefono());
            if (modifica.getDni().isEmpty())
                throw new ValidationException("Debe ingresar el dni");
            else update.setDni(modifica.getDni());
            medicoRepository.save(update);
            return new ResponseEntity<>("Se actualizo", HttpStatus.OK);
        }else
           /* return new ResponseEntity<>("Medico no existente", HttpStatus.NOT_FOUND);*/
        throw new NotFoundException("NO existe el medico");

    }


    public Page<MedicoDto> listarTodosByPage(Integer pageNumber, Integer pageSize, String orderField) {
        Pageable pageable= PageRequest.of(pageNumber, pageSize, Sort.by(orderField));
        return medicoRepository.findAll(pageable).map(medicoMapper::entityToDto);
    }

}
