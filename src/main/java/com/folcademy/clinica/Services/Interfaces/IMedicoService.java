package com.folcademy.clinica.Services.Interfaces;


import com.folcademy.clinica.Model.Dtos.MedicoDto;
import org.springframework.data.domain.Page;

public interface IMedicoService {
       // List<MedicoDTO> findAll();
        Page<MedicoDto> findAll(Integer pageNumber, Integer pageSize, String orderField);
        Page<MedicoDto> findById(Integer id,Integer pageNumber, Integer pageSize, String orderField);
}