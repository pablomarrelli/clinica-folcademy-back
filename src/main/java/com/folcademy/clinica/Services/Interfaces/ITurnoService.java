package com.folcademy.clinica.Services.Interfaces;

import com.folcademy.clinica.Model.Dtos.TurnoDto;
import com.folcademy.clinica.Model.Entities.Turno;
import org.springframework.data.domain.Page;

import java.util.List;

public interface ITurnoService {
    Page<TurnoDto> findAll(Integer pageNumber, Integer pageSize, String orderField);
    Page<TurnoDto> findById(Integer id, Integer pageNumber, Integer pageSize, String orderField);
    /*List<Turno> findTurnoById(Integer id);*/
}
