package com.folcademy.clinica.Services.Interfaces;

import com.folcademy.clinica.Model.Dtos.PacienteDto;
import com.folcademy.clinica.Model.Entities.Paciente;
import org.springframework.data.domain.Page;

import java.util.List;

public interface IPacienteService {

    Page<PacienteDto> findAll(Integer pageNumber, Integer pageSize, String orderField);
    Page<PacienteDto> findById(Integer id,Integer pageNumber, Integer pageSize, String orderField);


}
