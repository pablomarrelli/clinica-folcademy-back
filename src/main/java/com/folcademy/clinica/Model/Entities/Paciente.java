package com.folcademy.clinica.Model.Entities;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.Hibernate;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.util.Objects;

@Entity(name = "paciente")
@Getter
@Setter
@ToString
@RequiredArgsConstructor
public class Paciente extends Persona {


    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idpaciente"/*, columnDefinition = "INT(10) UNSIGNED"*/)
    public Integer idpaciente;


    @Column(name = "direccion"/*, columnDefinition = "VARCHAR"*/)
    String direccion= "";

    @Column(name = "idpersona",columnDefinition = "INT")
    public Integer idpersona;

    @OneToOne
    @NotFound(action = NotFoundAction.IGNORE)/*Si es null el id paciente lo ignoramos*/
    @JoinColumn(name = "idpersona", referencedColumnName = "idpersona" ,insertable = false , updatable = false)/*primer id paciente hace referencia a la tabla turnos yy el segundo a la tabla paciente*/
            Persona persona;

    // @Column(name = "dni"/*, columnDefinition = "VARCHAR"*/)
    //String dni ="";

    /*@Column(name = "nombre", columnDefinition = "VARCHAR")
    String nombre= "";*/

    /*@Column(name = "apellido", columnDefinition = "VARCHAR")
    String apellido= "";*/

    /*@Column(name = "telefono"/*, columnDefinition = "VARCHAR")
    String telefono= "";*/


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Paciente paciente = (Paciente) o;
        return idpaciente!= null && Objects.equals(idpaciente, paciente.idpaciente);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
