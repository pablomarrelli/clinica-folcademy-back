package com.folcademy.clinica.Model.Entities;

import lombok.*;
import org.hibernate.Hibernate;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Objects;

@Entity
@Table(name = "turno")
@Getter
@Setter
@ToString
@RequiredArgsConstructor
public class Turno {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name= "idturno", columnDefinition = "INT(10)UNSIGNED")
    Integer id;

    @Column(name = "fecha", columnDefinition = "DATE")
    public LocalDate fecha;

    @Column(name = "hora", columnDefinition = "TIME")
    public LocalTime hora;

    @Column(name = "atendido", columnDefinition = "TINYINT")
    Boolean atendido;

    @Column(name = "idmedico", columnDefinition = "INT")
    public Integer idmedico;


    @Column(name = "idpaciente", columnDefinition = "INT")
    public Integer idpaciente;

    @ManyToOne
    @NotFound(action = NotFoundAction.IGNORE)/*Si es null el id paciente lo ignoramos*/
    @JoinColumn(name = "idpaciente", referencedColumnName = "idpaciente" ,insertable = false, updatable = false)/*primer id paciente hace referencia a la tabla turnos yy el segundo a la tabla paciente*/
    private Paciente paciente;

    @ManyToOne
    @NotFound(action = NotFoundAction.IGNORE)/*Si es null el id paciente lo ignoramos*/
    @JoinColumn(name = "idmedico", referencedColumnName = "idmedico" ,insertable = false , updatable = false)/*primer id paciente hace referencia a la tabla turnos yy el segundo a la tabla paciente*/
    private Medico medico;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Turno turno = (Turno) o;
        return id != null && Objects.equals(id, turno.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
