package com.folcademy.clinica.Model.Entities;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Getter
@Setter
@RequiredArgsConstructor
@ToString
@Table(name = "persona")
@Inheritance(strategy = InheritanceType.JOINED)
public class Persona {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idpersona"/*, columnDefinition = "INT(10) UNSIGNED"*/)
    Integer idpersona;

    @Column(name= "nombre"/*, columnDefinition = "INT(10)UNSIGNED"*/)
    String nombre = "";

    @Column(name= "apellido"/*, columnDefinition = "INT(10)UNSIGNED"*/)
    String apellido = "";

    @Column(name = "dni"/*, columnDefinition = "VARCHAR"*/)
    String dni ="";

    @Column(name = "telefono"/*, columnDefinition = "VARCHAR"*/)
    String telefono= "";

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Persona persona = (Persona) o;
        return idpersona != null && Objects.equals(idpersona, persona.idpersona);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }

}
