package com.folcademy.clinica.Model.Entities;

import lombok.*;
import org.hibernate.Hibernate;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "medico")
@Getter
@Setter
@ToString
@RequiredArgsConstructor
public class Medico extends Persona{


    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name= "idmedico"/*, columnDefinition = "INT(10)UNSIGNED"*/)
    Integer IdMedico;

    @Column(name= "profesion"/*, columnDefinition = "INT(10)UNSIGNED"*/)
    String profesion = "";

    @Column(name= "consulta"/*, columnDefinition = "INT(10)UNSIGNED"*/)
    int consulta = 0;

    @Column(name= "idpersona"/*, columnDefinition = "INT(10)UNSIGNED"*/)
    Integer idpersona;

    @OneToOne
    @NotFound(action = NotFoundAction.IGNORE)/*Si es null el id lo ignoramos*/
    @JoinColumn(name = "idpersona", referencedColumnName = "idpersona" ,insertable = false , updatable = false)
    Persona persona;

    /*@Column(name = "dni"/*, columnDefinition = "VARCHAR")
    String dni ="";*/

    /*@Column(name = "telefono"/*, columnDefinition = "VARCHAR")
    String telefono= "";*/
     /* @Column(name= "nombre"/*, columnDefinition = "INT(10)UNSIGNED")
    String nombre = ""*/

    /*@Column(name= "apellido"/*, columnDefinition = "INT(10)UNSIGNED")
    String apellido = "";*/

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Medico medico = (Medico) o;
        return IdMedico!= null && Objects.equals(IdMedico, medico.IdMedico);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
