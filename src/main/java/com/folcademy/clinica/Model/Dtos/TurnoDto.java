package com.folcademy.clinica.Model.Dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TurnoDto {

    Integer id;

    LocalDate fecha;

    LocalTime hora;

    Boolean atendido;

    Integer idmedico;

    Integer idpaciente;

}
