package com.folcademy.clinica.Model.Dtos;

import com.folcademy.clinica.Model.Entities.Persona;
import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MedicoDto{
    @NotNull
    Integer IdMedico;


    @NotNull
    String profesion;

    int consulta;

    @NotNull
    Persona persona;

    //String nombre;
    /*String dni;

    String tel;*/
}
