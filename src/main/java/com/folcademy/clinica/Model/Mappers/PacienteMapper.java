package com.folcademy.clinica.Model.Mappers;

import com.folcademy.clinica.Model.Dtos.PacienteDto;
import com.folcademy.clinica.Model.Entities.Paciente;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class PacienteMapper {
    public PacienteDto entityToDto(Paciente entity){
        return Optional
                .ofNullable(entity)
                .map(
                        ent -> new PacienteDto(
                                ent.getIdpaciente(),
                                ent.getDireccion(),
                                ent.getPersona()
                                )
                )
                .orElse(new PacienteDto());

    }

    public Paciente dtoToEntity(PacienteDto dto){
        Paciente entity = new Paciente();
        entity.setIdpaciente(dto.getId());
        entity.setPersona(dto.getPersona());
        entity.setDireccion(dto.getDireccion());
        return entity;
    }
}
