package com.folcademy.clinica.Model.Mappers;

import com.folcademy.clinica.Model.Dtos.MedicoDto;
import com.folcademy.clinica.Model.Entities.Medico;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class MedicoMapper {
    public MedicoDto entityToDto(Medico entity){
        return Optional
                .ofNullable(entity)
                .map(
                        ent -> new MedicoDto(
                                ent.getIdMedico(),
                                ent.getProfesion(),
                                ent.getConsulta(),
                                ent.getPersona()
                                //ent.getNombre()
                        )
                )
                .orElse(new MedicoDto());

    }

    public Medico dtoToEntity(MedicoDto dto){
        Medico entity = new Medico();
        entity.setIdMedico(dto.getIdMedico());
        entity.setProfesion(dto.getProfesion());
        entity.setConsulta(dto.getConsulta());
        //entity.setNombre(dto.getNombre());
        entity.setPersona(dto.getPersona());
        return entity;
    }

}
