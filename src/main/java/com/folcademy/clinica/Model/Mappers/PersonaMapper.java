package com.folcademy.clinica.Model.Mappers;


import com.folcademy.clinica.Model.Dtos.PersonaDto;
import com.folcademy.clinica.Model.Entities.Persona;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class PersonaMapper {
    public PersonaDto entityToDto(Persona entity){
        return Optional
                .ofNullable(entity)
                .map(
                        ent -> new PersonaDto(
                                ent.getIdpersona(),
                                ent.getDni(),
                                ent.getNombre(),
                                ent.getApellido(),
                                ent.getTelefono()
                        )
                )
                .orElse(new PersonaDto());

    }

    public Persona dtoToEntity(PersonaDto dto){
        Persona entity = new Persona();
        entity.setIdpersona(dto.getId());
        entity.setDni(dto.getDni());
        entity.setTelefono(dto.getTel());
        entity.setApellido(dto.getApellido());
        entity.setNombre(dto.getNombre());
        return entity;
    }

}

