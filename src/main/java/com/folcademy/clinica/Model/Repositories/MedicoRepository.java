package com.folcademy.clinica.Model.Repositories;

import com.folcademy.clinica.Model.Dtos.MedicoDto;
import com.folcademy.clinica.Model.Entities.Medico;
import com.folcademy.clinica.Model.Entities.Persona;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;


@Repository("medicoRepository")
public interface MedicoRepository extends PagingAndSortingRepository <Medico, Integer> {
    Page<Medico> findAll(Pageable pageable);
    Page<Medico> findByIdMedico(Integer IdMedico, Pageable pageable);
    Medico getById(Integer idMedico);
}

