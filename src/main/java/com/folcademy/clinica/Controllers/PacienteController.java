package com.folcademy.clinica.Controllers;

import com.folcademy.clinica.Model.Dtos.MedicoDto;
import com.folcademy.clinica.Model.Dtos.PacienteDto;
import com.folcademy.clinica.Model.Entities.Paciente;
import com.folcademy.clinica.Services.PacienteService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/paciente")
public class PacienteController {

    private final PacienteService pacienteService;

    public PacienteController(PacienteService pacienteService) {
        this.pacienteService = pacienteService;
    }


  /*  @GetMapping(value = "")
    public ResponseEntity<List<Paciente>> findAll() {
        return ResponseEntity
                .ok()
                .body(
                        pacienteService.findAllPacientes())
                ;
    }*/

    //@PreAuthorize("hasAuthority('get')")
    @GetMapping("")
    public ResponseEntity<Page<PacienteDto>> ListaPacientes(
            @RequestParam(name = "pageNumber", defaultValue = "1") Integer pageNumber,
            @RequestParam(name = "pageSize", defaultValue = "2") Integer pageSize,
            @RequestParam(name = "orderField", defaultValue ="apellido" ) String orderField
    ) {
        return ResponseEntity.ok(pacienteService.ListaPacientes(pageNumber, pageSize, orderField));
    }

    /*@GetMapping(value = "/{id}")
    public ResponseEntity<List<Paciente>> findAll(@PathVariable(name = "id") Integer id) {
        return ResponseEntity
                .ok()
                .body(
                        pacienteService.findPacienteById(id))
                ;
    }*/

    @GetMapping("/page")
    public ResponseEntity<Page<PacienteDto>> listarTodoByPage(
            @RequestParam(name = "pageNumber", defaultValue = "1") Integer pageNumber,
            @RequestParam(name = "pageSize", defaultValue = "2") Integer pageSize,
            @RequestParam(name = "orderField", defaultValue ="apellido" ) String orderField
    ) {

        return ResponseEntity.ok(pacienteService.listarTodosByPage(pageNumber, pageSize, orderField));
    }

    //@PreAuthorize("hasAuthority('get')")
    @GetMapping("/{idPaciente}")
    public ResponseEntity<Page<PacienteDto>> findById(
            @PathVariable(name = "idPaciente") Integer id,
            @RequestParam(name = "pageNumber", defaultValue = "1") Integer pageNumber,
            @RequestParam(name = "pageSize", defaultValue = "2") Integer pageSize,
            @RequestParam(name = "orderField", defaultValue ="domicilio" ) String orderField
    ){
        return ResponseEntity
                .ok()
                .body(pacienteService.findById(id, pageNumber, pageSize, orderField));
    }
//
    //@PreAuthorize("hasAuthority('post')")
    @PostMapping("")
    public ResponseEntity<PacienteDto> agregar(@RequestBody @Validated PacienteDto paciente){
        return ResponseEntity.ok(pacienteService.agregar(paciente));
    }

    @DeleteMapping("/{idPaciente}")
    public ResponseEntity<PacienteDto> borrar(@PathVariable(name = "idPaciente")int id)
    {
        return new ResponseEntity(pacienteService.borrar(id), HttpStatus.OK);
    }

    @PutMapping("/{idPaciente}")
    public ResponseEntity<String> update(@PathVariable ("idPaciente") Integer id, @RequestBody PacienteDto modifica)
    {
        return (pacienteService.update(id, modifica));
    }

}
