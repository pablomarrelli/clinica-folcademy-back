package com.folcademy.clinica.Controllers;

import com.folcademy.clinica.Model.Dtos.MedicoDto;
import com.folcademy.clinica.Model.Dtos.PacienteDto;
import com.folcademy.clinica.Model.Entities.Medico;
import com.folcademy.clinica.Model.Entities.Persona;
import com.folcademy.clinica.Services.MedicoService;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
//import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.util.List;

@RestController
@RequestMapping("/medico")
public class MedicoController {

    private final MedicoService medicoService;

    public MedicoController(MedicoService medicoService){
        this.medicoService = medicoService;
    }

    ///@PreAuthorize("hasAuthority('get')")
    @GetMapping("")
    public ResponseEntity<Page<MedicoDto>> listarTodo(
            @RequestParam(name = "pageNumber", defaultValue = "1") Integer pageNumber,
            @RequestParam(name = "pageSize", defaultValue = "2") Integer pageSize,
            @RequestParam(name = "orderField", defaultValue ="apellido" ) String orderField
    ) {
        return ResponseEntity.ok(medicoService.listarTodos(pageNumber, pageSize, orderField));
    }

    @GetMapping("/page")
    public ResponseEntity<Page<MedicoDto>> listarTodoByPage(
            @RequestParam(name = "pageNumber", defaultValue = "1") Integer pageNumber,
            @RequestParam(name = "pageSize", defaultValue = "2") Integer pageSize,
            @RequestParam(name = "orderField", defaultValue ="apellido" ) String orderField
    ) {

        return ResponseEntity.ok(medicoService.listarTodosByPage(pageNumber, pageSize, orderField));
    }

    //@PreAuthorize("hasAuthority('get')")
    @GetMapping("/{idMedico}")
    public ResponseEntity<Page<MedicoDto>> listarUno(
            @PathVariable(name = "idMedico") Integer id,
            @RequestParam(name = "pageNumber", defaultValue = "0") Integer pageNumber,
            @RequestParam(name = "pageSize", defaultValue = "1") Integer pageSize,
            @RequestParam(name = "orderField", defaultValue ="profesion" ) String orderField)
    {
        return ResponseEntity.ok(medicoService.listarUno(id, pageNumber, pageSize, orderField));
    }

    //@PreAuthorize("hasAuthority('post')")
    @PostMapping("")
    public ResponseEntity<MedicoDto> agregar(@RequestBody @Validated MedicoDto entity){
        return ResponseEntity.ok(medicoService.agregar(entity));
    }

    @DeleteMapping("/{idMedico}")
    public ResponseEntity<MedicoDto> borrar(@PathVariable(name= "idMedico")int id){

        return new ResponseEntity(medicoService.borrar(id),HttpStatus.OK);
    }

    /*@PutMapping("/{idMedico}")
    public ResponseEntity<String> updateProfesion(@PathVariable ("idMedico") int id, @RequestParam String profesion)
    {
        return (medicoService.updateProfesion(id, profesion));
    }
*/
    @PutMapping("/{idMedico}")
    public ResponseEntity<String> update(@PathVariable ("idMedico") Integer id, @RequestBody Medico modifica)
    {
        return (medicoService.update(id, modifica));
    }


}
