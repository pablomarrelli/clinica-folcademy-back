package com.folcademy.clinica.Controllers;

import com.folcademy.clinica.Model.Dtos.MedicoDto;
import com.folcademy.clinica.Model.Dtos.PacienteDto;
import com.folcademy.clinica.Model.Dtos.TurnoDto;
import com.folcademy.clinica.Model.Entities.Paciente;
import com.folcademy.clinica.Model.Entities.Turno;
import com.folcademy.clinica.Services.TurnoService;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/turno")
public class TurnoController {

    private final TurnoService turnoService;

    public TurnoController(TurnoService turnoService) {
        this.turnoService = turnoService;
    }
    @GetMapping("")

    public ResponseEntity<Page<TurnoDto>> findAll(
            @RequestParam(name = "pageNumber", defaultValue = "1") Integer pageNumber,
            @RequestParam(name = "pageSize", defaultValue = "2") Integer pageSize,
            @RequestParam(name = "orderField", defaultValue ="apellido" ) String orderField
    ) {
        return ResponseEntity.ok(turnoService.findAll(pageNumber, pageSize, orderField));
    }
  /* @GetMapping("")
   public ResponseEntity<List<TurnoDto>> ListaTurnos(){return ResponseEntity.ok(turnoService.ListaTurnos());}
*/
    //
   /* @GetMapping(value = "/{id}")
   public ResponseEntity<List<Turno>> findAll(@PathVariable(name = "id") Integer id) {
       return ResponseEntity
               .ok()
               .body(
                       turnoService.findTurnoById(id))
               ;
   }*/

    @GetMapping("/{idTurno}")
    public ResponseEntity<Page<TurnoDto>> findById(
            @PathVariable(name = "idTurno") Integer id,
            @RequestParam(name = "pageNumber", defaultValue = "0") Integer pageNumber,
            @RequestParam(name = "pageSize", defaultValue = "1") Integer pageSize,
            @RequestParam(name = "orderField", defaultValue ="id" ) String orderField)
    {
        return ResponseEntity
                .ok()
                .body(
                        turnoService.findById(id, pageNumber, pageSize, orderField));
    }

    @PostMapping("")
    public ResponseEntity<TurnoDto> agregar(@RequestBody @Validated TurnoDto turno){
        return ResponseEntity.ok(turnoService.insertar(turno));
    }

    @DeleteMapping("/{idturno}")
    public ResponseEntity<TurnoDto> borrar(@PathVariable(name = "idturno")int id)
    {
        return new ResponseEntity(turnoService.borrar(id), HttpStatus.OK);
    }
    @PutMapping("/{idturno}")
    public ResponseEntity<String> update(@PathVariable ("idturno") Integer id, @RequestBody TurnoDto modifica)
    {
        return (turnoService.update(id, modifica));
    }


}
